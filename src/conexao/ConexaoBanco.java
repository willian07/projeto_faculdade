package conexao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

import model.usuario;
//conexao com o banco de dados
public class ConexaoBanco {

    private static final String USUARIO = "root";
    private static final String SENHA = "willian07";
    private static final String URL = "jdbc:mysql://127.0.0.1:3306/willian_teste";
    private static final String DRIVER = "com.mysql.jdbc.Driver";

    // Conectar ao banco
    public static Connection abrir() throws Exception {
    	Connection conn = null;
        // Registrar o driver
    	try {
    		Class.forName(DRIVER);
            // Capturar a conex�o
    		conn = DriverManager.getConnection(URL, USUARIO, SENHA);
            // Retorna a conexao aberta
		} catch (Exception e) {
			// TODO: handle exception
		}
        return conn;
    }
    
    
    //responsavel por salvar o cadastro do novo usuario
    public static boolean  Salvar( String nome,String sobrenome,String cpf,String login,String senha, double peso,double altura,String empresa_filiada,String data_inicial,String email) throws Exception{
    	boolean result = false;
    	Connection conn = ConexaoBanco.abrir();
    	try {
    	PreparedStatement stmt = null;
    	stmt = conn.prepareStatement("insert into usuario(nome,sobrenome,cpf,login,senha,peso,altura,empresa_filiada,data_inicial,email) values(?,?,?,?,?,?,?,?,?,?)");
    	stmt.setString(1,nome);
    	stmt.setString(2,sobrenome);
    	stmt.setString(3,cpf);
    	stmt.setString(4,login);
    	stmt.setString(5,senha);
    	stmt.setDouble(6,peso);
    	stmt.setDouble(7,altura);
    	stmt.setString(8,empresa_filiada);
    	stmt.setString(9,data_inicial);
    	stmt.setString(10,email);
    	stmt.executeUpdate();
    	stmt.close();
    	result = true;
    	} catch (Exception e) {
			// TODO: handle exception
		}finally {
			conn.close();
		}
		return result;
    }
    //responsavel por validader o login
    public static boolean valida(String login,String senha) throws Exception{
    	String nome = "";
    	boolean result = false;
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select nome from usuario where login = "+"'"+login+"'"+" and senha = "+"'"+senha+"';";
    	try {
			Statement st = (Statement) conn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			if(rs.next()) {
				result = true;
				nome = rs.getString("nome");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return result;
    }
    //responsavel por slavar o lembrete no banco de dados
    public static boolean cadastra_agenda(String data, String msg,String login_usuario) throws Exception {
    	boolean result = false;
    	Connection conn = ConexaoBanco.abrir();
    	String data_inicial = system.funcoes_sistema.getDateTime();
    	try {
    	PreparedStatement stmt = null;
    	stmt = conn.prepareStatement("insert into agenda(data_agenda,data_inicio,msg,login_usuario) values(?,?,?,?)");
    	stmt.setString(1,data);
    	stmt.setString(2,data_inicial);
    	stmt.setString(3,msg);
    	stmt.setString(4,login_usuario);
    	stmt.executeUpdate();
    	stmt.close();
    	result = true;
    	} catch (Exception e) {
			result = false;
		}finally {
			conn.close();
		}
		return result;
    }
    //retorna o lembrete de acordo com usuario e data
    public static String retorna_lembrete(String login) throws Exception {
    	String msg= "";
    	String sql = "";
    	String data_inicial = system.funcoes_sistema.getDateTime();
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select msg from agenda where login_usuario = "+"'"+login+"'"+" and data_agenda = "+"date_format('"+data_inicial+"','%Y-%m-%d')";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
					msg = rs.getString("msg");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return msg;
    	
    }
    
    //retorna o lembrete de acordo com usuario e data
    public static String retorna_lembrete_empresa() throws Exception {
    	String msg= "";
    	String sql = "";
    	String data_inicial = system.funcoes_sistema.getDateTime();
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select msg from lembrete_empresa where data_agenda = "+"date_format('"+data_inicial+"','%Y-%m-%d')";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
					msg = rs.getString("msg");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return msg;
    	
    }
    //retorna dados do usuario
    public static String retorna_nome() throws Exception {
    	String nome = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select nome from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				nome = rs.getString("nome");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return nome;
    	
    }
  //retorna dados do usuario
    public static String retorna_sobrenome() throws Exception {
    	String sobrenome = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select sobrenome from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				sobrenome = rs.getString("sobrenome");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return sobrenome;
    	
    }
  //retorna dados do usuario
    public static String retorna_cpf() throws Exception {
    	String cpf = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select cpf from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				cpf = rs.getString("cpf");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return cpf;
    	
    }
  //retorna dados do usuario
    public static String retorna_senha() throws Exception {
    	String senha = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select senha from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				senha = rs.getString("senha");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return senha;
    	
    }
  //retorna dados do usuario
    public static String retorna_altura() throws Exception {
    	String altura = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select altura from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				altura = rs.getString("altura");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return altura;
    	
    }
    public static String retorna_empresa_filiada() throws Exception {
    	String empresa_filiada = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select empresa_filiada from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				empresa_filiada = rs.getString("empresa_filiada");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return empresa_filiada;
    	
    }
    public static String retorna_email() throws Exception {
    	String email = "";
    	String login= "willian123";
    	String sql = "";
    	Connection conn = ConexaoBanco.abrir();
    	sql += "select email from usuario where login = "+"'"+login+"'";
    	try {
			PreparedStatement st = conn.prepareStatement(sql);
			ResultSet rs = st.executeQuery(sql);
			while(rs.next()) {
				email = rs.getString("email");
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return email;
    	
    }

}